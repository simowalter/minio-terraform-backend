terraform {
  backend "s3" {
    endpoint      = "http://13.37.180.93:9000"
    bucket        = "bucket-minio"
    key           = "terraform.tfstate"
    # access_key = "xxxxxx"
    # secret_key = "xxxxxxxxxxxx"
    region = "main"
    skip_credentials_validation = true
    skip_metadata_api_check = true
    skip_region_validation = true
    force_path_style = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  required_version = ">= 1.2.0"
}

# COMMANDS Terminal:
# export MINIO_ACCESS_KEY="xxxxxxxxxx"
# export MINIO_SECRET_KEY="xxxxxxxxxx"

# terraform init -backend-config="access_key=$MINIO_ACCESS_KEY" -backend-config="secret_key=$MINIO_SECRET_KEY"
